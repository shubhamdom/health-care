# Generated by Django 2.2.8 on 2021-03-03 07:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('Admin', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PatientInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('p_id', models.CharField(max_length=30)),
                ('p_firstname', models.CharField(max_length=70)),
                ('p_middlename', models.CharField(max_length=64)),
                ('p_lastname', models.CharField(max_length=64)),
                ('p_email', models.CharField(max_length=60)),
                ('p_phone', models.CharField(max_length=70)),
                ('p_age', models.IntegerField()),
                ('p_address', models.CharField(max_length=100)),
                ('patient_pic', models.ImageField(default='default.jpg', upload_to='patientphoto')),
            ],
        ),
        migrations.CreateModel(
            name='appointment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app_id', models.CharField(max_length=60, null=True)),
                ('p_firstname', models.CharField(max_length=50)),
                ('p_middlename', models.CharField(max_length=64)),
                ('p_lastname', models.CharField(max_length=64)),
                ('p_email', models.CharField(max_length=60)),
                ('p_phone', models.CharField(max_length=60)),
                ('p_appointmentDate', models.CharField(max_length=20)),
                ('p_timeslot', models.CharField(choices=[('9:00am-12:00pm', '9:00-12:00'), ('3:00pm-5:00pm', '3:00pm-5:00pm'), ('7:00pm-9:00pm', '7:00pm-9:00pm')], max_length=60)),
                ('p_message', models.CharField(max_length=256)),
                ('p_status', models.BooleanField(default=False)),
                ('p_doctor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Admin.Doctors')),
                ('p_specialization', models.ForeignKey(max_length=64, on_delete=django.db.models.deletion.CASCADE, to='Admin.specializations')),
            ],
        ),
    ]
