# Generated by Django 3.1.5 on 2021-03-17 12:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Patient', '0006_support_email'),
    ]

    operations = [
        migrations.AlterField(
            model_name='support',
            name='email',
            field=models.EmailField(max_length=256),
        ),
    ]
