# Generated by Django 3.1.5 on 2021-03-09 07:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Patient', '0003_auto_20210305_1120'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='p_appointmentDate',
            field=models.DateField(max_length=20),
        ),
    ]
