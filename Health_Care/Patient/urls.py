from django.contrib import admin
from django.urls import path
from . import views
from django.conf.urls import url, include
urlpatterns = [
    path('',views.index,name='index'),
    path('registration/',views.registration,name='registration'),
    path('Patient_profile/<str:email>/',views.Patient_profile,name='Patient_profile'),
    path('login/',views.login,name='login'),
    path('Patient_appointment/<str:email>/',views.Patient_appointment,name='Patient_appointment'),
    path('Patient_history/<str:email>/',views.Patient_history,name='Patient_history'),
    path('P_dashboard/',views.P_dashboard,name='P_dashboard'),
    path('P_editProfile/<str:email>/',views.P_editprofile,name='P_editProfile'),
    path('ajax/load-doctor/', views.load_doctor, name='ajax_load_doctor'),
    path('dropdown/',views.dropdown,name='dropdown'),
    path('patient_logout/',views.patient_logout,name="patient_logout"),
    path('Patient_prescription/',views.Patient_prescription,name="Patient_prescription"),
    path('Patient_labTest/',views.Patient_labTest,name="Patient_labTest"),
    path('Patient_Support/',views.Patient_Support,name='Patient_Support'),
    path('Patient_paymentHistory/',views.Patient_paymentHistory,name='Patient_payment'),
    path('Patient_paymentSuccess/',views.Patient_paymentSuccess,name='Patient_paymentSuccess'),
    path('Patient_payAmount/',views.Patient_payAmount,name='Patient_payAmount'),
    path('Patient_changePassword/',views.Patient_changePassword,name='Patient_changePassword')
]