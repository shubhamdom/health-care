from django import forms
from django.forms import Textarea
from .models import Doctors
from Admin.models import User
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from .models import appointment ,PatientInfo,Support
import re
import datetime
from django.db.models import Q

from django.forms import ModelForm, PasswordInput,Textarea
class registrationForm(forms.ModelForm):
    class Meta:
        model = PatientInfo
        fields =  {'p_firstname','p_middlename','p_lastname','p_email','p_phone','p_age','p_address'}
        labels = {
            'p_firstname':'First Name',
            'p_middlename':'Middle Name',
            'p_lastname':'Last Name',
            'p_email':'Email',
            'p_phone':'Phone',
            'p_age':'Age',
            'p_address':'Address',
        }

        widgets={
            'p_firstname':forms.TextInput(attrs={'placeholder': 'Enter Your Name'}),
            'p_middlename':forms.TextInput(attrs={'placeholder':'Enter Middle Name'}),
            'p_lastname':forms.TextInput(attrs={'placeholder':'Enter Last Name'}),
            'p_age':forms.NumberInput(attrs={'placeholder':'Enter Your Age'}),
            'p_email':forms.TextInput(attrs={'placeholder':'Enter Your Email'}),
            'p_phone':forms.TextInput(attrs={'placeholder': 'Enter Your Phone Number'}),
            'p_address':forms.TextInput(attrs={'placeholder': 'Enter Your Address'}),
        }


    def clean_p_firstname(self):
        p_firstname = self.cleaned_data.get('p_firstname')
        if ' ' in p_firstname:
            raise  forms.ValidationError("Space is not allowed")
        elif p_firstname.isalpha()==False:
            raise forms.ValidationError("Enter only Charecter")
        return p_firstname
    def clean_p_middlename(self):
        p_middlename = self.cleaned_data.get('p_middlename')
        if ' ' in p_middlename:
            raise  forms.ValidationError("Space is not allowed")
        elif p_middlename.isalpha()==False:
            raise forms.ValidationError("Enter only Charecter")
        return p_middlename
    def clean_p_lastname(self):
        p_lastname = self.cleaned_data.get('p_lastname')
        if ' ' in p_lastname:
            raise  forms.ValidationError("Space is not allowed")
        elif p_lastname.isalpha()==False:
            raise forms.ValidationError("Enter only Charecter")
        return p_lastname
    def clean_p_email(self):
        p_email = self.cleaned_data.get('p_email')
        regex="^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$"
        if re.search(regex, p_email)==None:
            raise forms.ValidationError("Enter Correct Email")
        return p_email

    def clean_p_phone(self):
        p_phone=self.cleaned_data.get('p_phone')
        regex=("^[0-9]{10}$")
        if re.search(regex, p_phone) == None:
            raise forms.ValidationError("Enter Correct 10 digit Phone no")
        return p_phone
class signupForm(forms.ModelForm):
    password=forms.CharField(widget=forms.PasswordInput())
    confirm_password=forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model=User
        fields={'password'}
        widgets = {
            'password': PasswordInput(attrs={'placeholder': 'eg: Abcd@1234$'}),

        }
    def clean_password(self):
        cleaned_data = super(registrationForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        if password != confirm_password:
            self.add_error('confirm_password', "Password does not match")

        return cleaned_data

    def clean_password(self):

        password = self.cleaned_data.get('password')
        if len(password) < 8:
            raise forms.ValidationError("Make sure your password is at lest 8 letters")
        elif re.search('[0-9]', password) is None:
            raise forms.ValidationError("Make sure your password has a number in it")
        elif re.search('[A-Z]', password) is None:
            raise forms.ValidationError("Make sure your password has a capital letter in it")
        return password


class appointmentForm(forms.ModelForm):
    class Meta:
        model = appointment
        fields =  {'p_firstname','p_middlename','p_lastname','p_email','p_phone','p_appointmentDate','p_specialization','p_doctor','p_timeslot','p_message'}

        labels = {
            'p_firstname':'First Name',
            'p_middlename':'Middle Name',
            'p_lastname':'Last Name',
            'p_email':'Email',
            'p_phone':'Phone',
            'p_appointmentDate':'Date',
            'p_specialization' : 'Specialization',
            'p_doctor' :'Doctor',
            'p_timeslot':'Timeslot',
            'p_message' :'Message'

        }

        widgets={
            'p_firstname':forms.TextInput(attrs={'placeholder': 'Your Name'}),
            'p_email':forms.TextInput(attrs={'placeholder':'Email'}),
            'p_phone':forms.TextInput(attrs={'placeholder': 'Phone'}),
            'p_appointmentDate':forms.DateInput(attrs={'type':'date'}),
            'p_message':forms.Textarea(attrs={'placeholder': 'Message' ,'maxlength':'100'}),
       }
    def clean_p_firstname(self):
      p_firstname = self.cleaned_data.get('p_firstname')
      if ' 'in p_firstname:
          raise forms.ValidationError("space is not allowed")
      elif p_firstname.isalpha()==False:
          raise forms.ValidationError("Enter only Character")
      return p_firstname
    def clean_p_middlename(self):
        p_middlename = self.cleaned_data.get('p_middlename')
        if ' 'in p_middlename:
            raise forms.ValidationError("space is not allowed")
        elif p_middlename.isalpha()==False:
            raise forms.ValidationError("Enter only Character")
        return p_middlename
    def clean_p_lastname(self):
        p_lastname = self.cleaned_data.get('p_lastname')
        if ' ' in p_lastname:
            raise forms.ValidationError('space is not allowed')
        elif p_lastname.isalpha()==False:
            raise forms.ValidationError("Enter only Character")
        return p_lastname
    def __init__(self,*args,**kwargs):
        super(appointmentForm,self).__init__(*args,**kwargs)
        instance = getattr(self,'instance',None)
        if instance and instance.pk:
            self.fields['p_email'].widget.attrs['readonly']=True
    def clean_p_email(self):
        p_email = self.cleaned_data['p_email']
        regex="^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$"
        if re.search(regex, p_email)==None:
            raise forms.ValidationError("Enter Correct Email")
        return p_email
    def clean_p_appointmentDate(self):
        p_appointmentDate = self.cleaned_data['p_appointmentDate']
        today = datetime.date.today()
        if p_appointmentDate < today:
            raise forms.ValidationError('The date cannot be in the past!')
        return p_appointmentDate
    def clean_p_phone(self):
        p_phone=self.cleaned_data.get('p_phone')
        regex=("^[0-9]{10}$")
        if re.search(regex, p_phone) == None:
            raise forms.ValidationError("Enter Correct 10 digit Phone no")
        return p_phone

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['p_doctor'].queryset = Doctors.objects.none()

            if 'specification' in self.data:
                try:
                    specification_id = int(self.data.get('specification'))
                    self.fields['p_doctor'].queryset = Doctors.objects.filter(country_id=specification_id).order_by('name')
                except (ValueError, TypeError):
                    pass  # invalid input from the client; ignore and fallback to empty City queryset
            elif self.instance.pk:
                self.fields['p_doctor'].queryset = self.instance.specification.doctor_set.order_by('name')


class profileForm(forms.ModelForm):
    class Meta:
        model = PatientInfo
        fields = {'p_firstname','p_middlename','p_lastname','p_email','p_phone','p_age','p_address','patient_pic'}

        labels = {
            'p_firstname':'First Name',
            'p_middlename':'Middle Name',
            'p_lastname':'Last Name',
            'p_email':'Email',
            'p_phone':'Phone',
            'p_age':'Age',
            'p_address' :'Address',
            'patient_pic':'Upload Photo',
        }
    field_order = ['p_firstname','p_middlename','p_lastname','p_email','p_phone','p_age','p_address','patient_pic']
    def clean_p_phone(self):
        p_phone=self.cleaned_data.get('p_phone')
        regex=("^[0-9]{10}$")
        if re.search(regex, p_phone) == None:
            raise forms.ValidationError("Enter Correct 10 digit Phone no")
        return p_phone
    def __init__(self,*args,**kwargs):
        super(profileForm,self).__init__(*args,**kwargs)
        instance = getattr(self,'instance',None)
        if instance and instance.pk:
            self.fields['p_firstname'].widget.attrs['readonly']=True
            self.fields['p_middlename'].widget.attrs['readonly']=True
            self.fields['p_lastname'].widget.attrs['readonly']=True
            self.fields['p_email'].widget.attrs['readonly']=True

class SupportForm(forms.ModelForm):
    class Meta:
        model = Support
        fields =  {'issueSubject','issue'}
        labels = {
            'issueSubject':'Subject',
            'issue':'Issue',
        }
        widgets={
            'issueSubject':forms.TextInput(attrs={'placeholder': 'Write subject here.'}),
            'issue':forms.Textarea(attrs={'placeholder':'Write Your Issue here.','max_length':'100'}),
        }
class changePasswordForm(forms.Form):
    oldPassword_flag = True
    oldPassword = forms.CharField(label="Old Password", min_length=6, widget=forms.PasswordInput())
    newPassword = forms.CharField(label="New Password", min_length=6, widget=forms.PasswordInput())
    re_new_password = forms.CharField(label="Re-type New Password", min_length=6, widget=forms.PasswordInput())

    def set_oldPassword_flag(self):
        self.oldPassword_flag = False
        return 0
    def clean_oldPassword(self, *args, **kwargs):
        oldPassword = self.cleaned_data.get('oldPassword')
        if not oldPassword:
            raise forms.ValidationError("You must enter your old password.")
        if self.oldPassword_flag == False:
            # It raise the validation error that password entered by user does not match the actucal old password.
            raise forms.ValidationError("The old password that you have entered is wrong.")
        return oldPassword
