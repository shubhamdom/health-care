from django.db import models
from django.contrib.auth.models import User
from Admin.models import Doctors,specializations
from datetime import datetime, date
import uuid
# Create your models here.
class PatientInfo(models.Model):
    p_id = models.CharField(max_length=30)
    p_firstname= models.CharField(max_length=70)
    p_middlename = models.CharField(max_length=64)
    p_lastname = models.CharField(max_length=64)
    p_email = models.CharField(max_length=60)
    p_phone = models.CharField(max_length=70)
    p_age= models.IntegerField()
    p_address = models.CharField(max_length=100)
    patient_pic=models.FileField(upload_to='patientphoto',null=True)
class appointment(models.Model):
    app_id = models.CharField(max_length=60, null=True)
    timeslot_choice=(('9:00am-12:00pm','9:00-12:00'),('3:00pm-5:00pm','3:00pm-5:00pm'),('7:00pm-9:00pm','7:00pm-9:00pm'))
    p_firstname = models.CharField(max_length=50)
    p_middlename = models.CharField(max_length=64)
    p_lastname = models.CharField(max_length=64)
    p_email = models.CharField(max_length=60)
    p_phone = models.CharField(max_length=60)
    p_appointmentDate = models.DateField(max_length=20)
    p_specialization = models.ForeignKey(specializations,max_length=64,on_delete=models.CASCADE)
    p_doctor = models.ForeignKey(Doctors,on_delete=models.CASCADE,null=False)
    p_timeslot = models.CharField(max_length=60,choices=timeslot_choice)
    p_message = models.CharField(max_length=256)
    p_status = models.BooleanField(default=False)

class Support(models.Model):
    email = models.EmailField(max_length=256)
    issueDate = models.DateField(auto_now=True)
    issueSubject = models.CharField(max_length=64)
    issue = models.CharField(max_length=128)

class patientPayment(models.Model):
    email= models.EmailField(max_length=64)
    amount = models.IntegerField()
    payment_id = models.CharField(max_length=100)
    paid = models.BooleanField(default=False)
