from django.shortcuts import render,redirect,get_object_or_404
from django.shortcuts import render,redirect
from django.contrib.auth.models import auth
from django.contrib import messages
from .models import PatientInfo,appointment,Support,patientPayment
from Admin.models import Doctors,User
from .forms import appointmentForm,profileForm,registrationForm,signupForm,SupportForm,changePasswordForm
import razorpay
from django.http import HttpResponse

import uuid
from django.views.generic import ListView
from datetime import datetime

def index(request):
    context = {'doctors': Doctors.objects.all()}
    return render(request, 'Patient/index.html',context)
def Patient_profile(request,email):
    context = {'data':PatientInfo.objects.filter(p_email=email)}
    return render(request,'patient/Patient_profile.html',context)
def login(request):
    if request.method == 'POST':
        uname=request.POST['uname']
        password=request.POST['pass']
        user = auth.authenticate(request, username=uname, password=password)
        if user is not None and user.is_staff==True:
            auth.login(request, user)
            return  redirect('/dr_dash')
        if user is not None:
            auth.login(request,user)
            return redirect('/P_dashboard/')
        #user not exits
        if user is None:
            messages.success(request, "invalid credentials")
            return redirect('/login/')
    return render(request,'patient/login.html')

def registration(request):
    if request.method == "POST":
        form = registrationForm(request.POST,request.FILES)
        sform = signupForm(request.POST)
        if form.is_valid():
            if sform.is_valid():
                p_email = form.cleaned_data['p_email']
                p_firstname= form.cleaned_data['p_firstname']
                p_middlename = form.cleaned_data['p_middlename']
                p_lastname = form.cleaned_data['p_lastname']
                password = sform.cleaned_data['password']
                """amount =int('99')*100
                client = razorpay.Client(auth=("rzp_test_I7AaHJeO8y5taf","mTso6lAKt6uLYkpdio1d09uk"))
                payment = client.order.create({'amount':amount,'currency':'INR' ,'payment_capture':'0'})
                money = patientPayment(email=p_email,amount=amount,payment_id=payment['id'])
                return render(request,'Patient/Patient_payAmount.html',{'payment':payment})"""
                if User.objects.filter(email=p_email).exists():
                   messages.success(request, 'This Email is already Exists!')
                else:

                    User.objects.create_user(username=p_email,email=p_email,password=password,first_name=p_firstname,middle_name=p_middlename,last_name=p_lastname).save()
                    form.save()
                    messages.success(request,'You Registerd Sucessfully!')
                    return redirect('/Patient_payAmount/')
        else:
            return render(request,'patient/registration.html',{'form':form,'sform':sform})
    form = registrationForm()

    sform = signupForm()
    return render(request, 'patient/registration.html', {
    'form': form,'sform':sform
},)

def Patient_dashboard(request):
    context = {'doctor':Doctors.objects.all()}
    return render(request,'patient/Patient_dashboard.html',context)
def Patient_appointment(request,email):
    obj = get_object_or_404(PatientInfo,p_email=email)
    form = appointmentForm(request.POST or None,request.FILES or None,instance=obj)
    if request.method == "POST":
        form = appointmentForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request,'Thank You!Your Appointment booked Successfully.')
            return redirect('/P_dashboard/')
        else:
            return render(request,'patient/P_appointment.html',{'form':form})
    return render(request,'patient/P_appointment.html',{'form':form})
def Patient_history(request,email):
    context = {'appointments':appointment.objects.filter(p_email=email)}
    return render(request,'Patient/Patient_history.html',context)

def P_dashboard(request):
    data = request.user

    context ={

               'Patient':PatientInfo.objects.get(p_email=data.email),
               'lastappt':appointment.objects.filter(p_email=data.email).order_by('-id')[:1]
               }
    return render(request,"Patient/P_dashboardView.html",context)
def P_editprofile(request,email):
    obj = get_object_or_404(PatientInfo,p_email=email)
    form = profileForm(request.POST or None,request.FILES or None,instance=obj)
    if form.is_valid():
        form.save()
        messages.success(request, 'Your Profile Updated Successfully!')
        return redirect('/P_dashboard/')
    else:
        return render(request,'patient/p_editProfile.html',{'form':form})
    return render(request,'patient/P_editProfile.html',{'form':form})
def load_doctor(request):
    specification_id = request.GET.get('p_specialization')
    print(specification_id)
    doctors = Doctors.objects.filter(specialties_id=specification_id)
    print(doctors)
    return render(request, 'Patient/dropdown.html', {'doctor': doctors})
def dropdown(request):
    return render(request,"Patient/dropdown.html")
def DoctorListView(ListView):
    model=appointment
    context_object_name='appointment_doctor'
def patient_logout(request):
    auth.logout(request)
    return redirect('/login/')
def Patient_prescription(request):
    return render(request,'Patient/Patient_prescription.html')
def Patient_labTest(request):
    return render(request,'Patient/Patient_labTest.html')
def Patient_Support(request):
    form = SupportForm(request.POST or None,request.FILES or None)
    if request.method == "POST":
        form = SupportForm(request.POST,request.FILES)
        if form.is_valid():
            data=request.user
            email = data.email
            issueSubject= form.cleaned_data['issueSubject']
            issue = form.cleaned_data['issue']
            Support(email=email,issueSubject=issueSubject,issue=issue).save()
            messages.success(request,'Thank you! Your Issue Submitted. We will Solve your issue as soon as possible.')
            return redirect('/P_dashboard/')
        else:
          return render(request,'patient/Patient_Support.html',{'form':form})
    return render(request,'patient/Patient_Support.html',{'form':form})

def Patient_paymentHistory(request):
    return render(request,'Patient/Patient_paymentHistory.html')
def Patient_paymentSuccess(request):
    if request.method=='POST':
        form=request.POST['form']
        sform=request.POST['sform']
        p_email = form.cleaned_data['p_email']
        p_firstname= form.cleaned_data['p_firstname']
        p_middlename = form.cleaned_data['p_middlename']
        p_lastname = form.cleaned_data['p_lastname']
        password = sform.cleaned_data['password']
        User.objects.create_user(username=p_email,email=p_email,password=password,first_name=p_firstname,middle_name=p_middlename,last_name=p_lastname)
        form.save()
        return render(request,'Patient/Patient_paymentSuccess.html')
def Patient_payAmount(request):
    if request.method == "POST":
        form = registrationForm(request.POST,request.FILES)
        sform = signupForm(request.POST)
        if form.is_valid():
            if sform.is_valid():
                p_email = form.cleaned_data['p_email']
                p_firstname= form.cleaned_data['p_firstname']
                p_middlename = form.cleaned_data['p_middlename']
                p_lastname = form.cleaned_data['p_lastname']
                password = sform.cleaned_data['password']
                p_age = form.cleaned_data['p_age']
                p_phone =  form.cleaned_data['p_phone']
                p_address = form.cleaned_data['p_address']

                amount =int('99')*100
                client = razorpay.Client(auth=("rzp_test_2FGwbDK4WN6BAU","DNX1eT4v12VVOI7MS1byltRo"))
                payment = client.order.create({'amount':amount,'currency':'INR' ,'receipt':"order_rcptid_11",'payment_capture':'0'})
                patientPayment(email=p_email,amount=amount,payment_id=payment['id']).save()
                payment_id=payment['id']
                payment_status=payment['status']
                if payment_status=="created":
                    context={'form':form,'sform':sform,'email':p_email,'first_name':p_firstname,'middle_name':p_middlename,'last_name':p_lastname,
                            'password':password,'age':p_age,'phone':p_phone,'address':p_address,'amount':amount,'order_id':payment_id}
                    return  render(request,'Patient/Patient_payAmount.html',context)
        else:
            return render(request,'patient/registration.html',{'form':form,'sform':sform})
    return HttpResponse('<h1>Invalid User Creation<h1>')

def Patient_changePassword(request):
    form = changePasswordForm(request.POST or None)

    oldPassword = request.POST.get("oldPassword")
    newPassword = request.POST.get("newPassword")
    re_new_password = request.POST.get("re_new__password")

    if request.POST.get("oldPassword"):

        user = User.objects.get(username=request.user.username)
        if user.check_password('{}'.format(oldPassword)) == False:
            form.set_oldPassword_flag()

    if form.is_valid():

        user.set_password('{}'.format(newPassword))
        user.save()
        messages.success(request, 'Successfully Change the Password')
        return redirect('/login/')

    return render(request, 'Patient/Patient_changePassword.html', {"form": form})