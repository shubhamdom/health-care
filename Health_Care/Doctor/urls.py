from django.contrib import admin
from django.urls import path
from . import views
from Patient.models import PatientInfo


urlpatterns = [
    path('dr_base/',views.base,name="dr_base"),
    path('dr_dash/',views.dash,name="dr_dash"),
    path('dr_profile/',views.profile,name="dr_profile"),
    #path('dr_createprofile/',views.createprofile,name="dr_createprofile"),
    path('dr_editprofile/<str:email>',views.editprofile,name="dr_editprofile"),
    #path('dr_updateprofile/<str:email>',views.updateprofile,name="dr_updateprofile"),
    path('dr_patients/',views.patients,name="dr_patients"),
    path('dr_delete_patient/<int:id>',views.delete_patient,name="dr_delete_patient"),
    path('dr_addpatient/',views.addpatient,name="dr_addpatient"),
    path('dr_editpatient/<int:id>',views.editpatient,name="dr_editpatient"),
    #path('dr_appointments/',views.appointments,name="dr_appointments"),
    path('dr_appointments/',views.appointments,name="dr_appointments"),
    path('dr_delete_appointment/<int:id>',views.dr_delete_appointment,name="dr_delete_appointment"),
    path('dr_edit_appointments/<int:id>',views.edit_appointments,name="dr_edit_appointments"),
    path('dr_today_appointments/<int:id>',views.today_appointments,name="dr_today_appointments"),
    path('dr_add_appointments/<str:email>',views.add_appointments,name="dr_add_appointments"),
    path('dr_schedule/',views.schedule,name="dr_schedule"),
    path('dr_add_schedule/',views.add_schedule,name="dr_add_schedule"),
    path('dr_edit_schedule/',views.edit_schedule,name="dr_edit_schedule"),
    path('dr_reports/',views.reports,name="dr_reports"),
    path('dr_prescription/',views.prescription,name="dr_prescription"),
    path('add_medicine/',views.add_medicine,name="add_medicine"),
    path('dr_bank_details/',views.dr_bank_details,name="dr_bank_details"),
    path('dr_logout/',views.dr_logout,name="dr_logout")
]