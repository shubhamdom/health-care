from django.db import models

# Create your models here.

class Prescription(models.Model):
    medicine_name = models.CharField(max_length=40)
    medicine_timing = models.CharField(max_length=40)
    medicine_quantity = models.IntegerField(null=True)
