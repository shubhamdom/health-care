from django import forms
from django.forms import ModelForm, PasswordInput,Textarea
from Admin.models import Doctors
from Patient.models import appointment
from .models import Prescription
from datetime import datetime
from django.contrib.auth.models import User
import re


class DoctorprofileForm(forms.ModelForm):
    class Meta:
        model = Doctors
        fields = {'first_name','last_name', 'email','age','gender','address','city','state','postal_code','qulifications','specialties','phone','photo','Certificate','bio'}
        widgets = {
            'bio': Textarea(),
            #'qulifications': Textarea()
            # 'email': errors_message={'required':'Enter email '},
        }

    field_order = ['first_name','last_name', 'email','age','gender','address','city','state' ,'postal_code','qulifications','specialties','phone','photo','Certificate','bio']

    """def clean_email(self):
        email = self.cleaned_data['email']
        regex="^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$"
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("Email already exists")
        elif re.search(regex, email)==None:
            raise forms.ValidationError("Enter Correct Email")
        return email
    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')
        if ' ' in first_name:
            raise  forms.ValidationError("Space is not allowed")
        elif first_name.isalpha()==False:
            raise forms.ValidationError("Enter only Charecter")
        return first_name
    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name')
        if ' ' in last_name:
            raise  forms.ValidationError("Space is not allowed")
        elif last_name.isalpha()==False:
            raise forms.ValidationError("Enter only Charecter")
        return last_name"""
    def clean_age(self):
        age = self.cleaned_data.get('age')
        if age<=22 or age>=65:
            raise forms.ValidationError("Enter Correct age")
        return age
    def clean_city(self):
        city = self.cleaned_data.get('city')
        if city.isalpha() == False:
            raise forms.ValidationError("Enter valid city name or only Charecter")
        return city
    def clean_state(self):
        state = self.cleaned_data.get('state')
        if state.isalpha() == False:
            raise forms.ValidationError("Enter valid city name or only Charecter")
        return state
    def clean_postal_code(self):
        postal_code=self.cleaned_data.get('postal_code')
        if len(str(postal_code))!=6:
            raise forms.ValidationError("Enter 6 digit valid postal Code ")
        return postal_code
    def clean_phone(self):
        phone=self.cleaned_data.get('phone')
        regex=("^[0-9]{10}$")
        if re.search(regex, phone) == None:
            raise forms.ValidationError("Enter Correct 10 digit Phone no")
        """elif Doctors.objects.filter(phone=phone).exists():
            raise forms.ValidationError("Phone no is already registered")"""
        return phone
    """def clean_bio(self):
        bio=self.cleaned_data.get('bio')
        if bio.count(' ') < 24:
            raise forms.ValidationError("Minimum 25 word are required ")
        return bio"""

    def __init__(self, *args, **kwargs):
        super(DoctorprofileForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['email'].widget.attrs['readonly'] = True
            self.fields['first_name'].widget.attrs['readonly'] = True
            self.fields['last_name'].widget.attrs['readonly'] = True
            self.fields['qulifications'].widget.attrs['readonly'] = True

    def clean_email(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.email
        else:
            return self.cleaned_data['email']


    def clean_first_name(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.first_name
        else:
            return self.cleaned_data['first_name']

    def clean_last_name(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.last_name
        else:
            return self.cleaned_data['last_name']

    def clean_qulifications(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.qulifications
        else:
            return self.cleaned_data['qulifications']



class PrescriptionForm(forms.ModelForm):
    class Meta:
        model = Prescription
        fields={'medicine_name','medicine_timing','medicine_quantity'}

    field_order = ['medicine_name','medicine_timing','medicine_quantity']

class AppointForm(forms.ModelForm):
    class Meta:
        model = appointment
        fields =  {'p_email','p_appointmentDate','p_timeslot','p_message'}

        labels = {
            'p_email':'Email',
            'p_appointmentDate':'Date',
            'p_timeslot':'Timeslot',
            'p_message' :'Message'

        }

        widgets={
            'p_email':forms.TextInput(attrs={'placeholder':'Email'}),
            'p_appointmentDate':forms.DateInput(attrs={'placeholder':'YYYY-MM-DD'}),

            'p_message':forms.Textarea(attrs={'placeholder': 'Message'}),
       }
class AddAppointmentForm(forms.ModelForm):
    class Meta:
        model = appointment
        fields = { 'p_email', 'p_phone', 'p_appointmentDate', 'p_timeslot',
                  'p_message'}

        labels = {

            'p_email': 'Email',
            'p_phone': 'Phone',
            'p_appointmentDate': 'Date',
            #'p_specialization': 'Specialization',
            #'p_doctor': 'Doctor',
            'p_timeslot': 'Timeslot',
            'p_message': 'Message'

        }

        widgets = {

            'p_email': forms.TextInput(attrs={'placeholder': 'Email'}),
            'p_phone': forms.TextInput(attrs={'placeholder': 'Phone'}),
            'p_appointmentDate': forms.TextInput(attrs={'type': 'date'}),

            'p_message': forms.Textarea(attrs={'placeholder': 'Message'}),
        }

    def clean_date(self):
        date = self.cleaned_data['p_appointmentDate']
        if date < datetime.date.today():
            raise forms.ValidationError("The date cannot be in the past!")
        return date

        """def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['p_doctor'].queryset = Doctors.objects.none()

            if 'specification' in self.data:
                try:
                    specification_id = int(self.data.get('specification'))
                    self.fields['p_doctor'].queryset = Doctors.objects.filter(country_id=specification_id).order_by(
                        'name')
                except (ValueError, TypeError):
                    pass  # invalid input from the client; ignore and fallback to empty City queryset
            elif self.instance.pk:
                self.fields['p_doctor'].queryset = self.instance.specification.doctor_set.order_by('name')"""

    def clean_email(self):
        email = self.cleaned_data['p_email']
        regex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$"
        if User.objects.filter(p_email=email).exists():
            raise forms.ValidationError("Email already exists")
        elif re.search(regex, email) == None:
            raise forms.ValidationError("Enter Correct Email")
        return email

    def clean_first_name(self):
        name = self.cleaned_data.get('p_name')
        if name.isalpha()==False:
            raise forms.ValidationError("Enter only Charecter")
        return name

    def clean_phone(self):
        p_phone=self.cleaned_data.get('p_phone')
        regex=("^[0-9]{10}$")
        if re.search(regex, p_phone) == None:
            raise forms.ValidationError("Enter Correct 10 digit Phone no")
        return p_phone