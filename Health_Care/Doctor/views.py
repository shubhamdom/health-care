from django.shortcuts import render, redirect, get_object_or_404, HttpResponseRedirect
from Patient.models import PatientInfo
from Patient.models import appointment
from .forms import DoctorprofileForm, AppointForm, AddAppointmentForm
from Admin.models import Doctors
from  .models import Prescription
from django.contrib.auth.models import auth, User
from datetime import datetime, timedelta

# Create your views here.
def base(request):
    return render(request,'Doctor/base.html')
def dash(request):
    data = request.user
    today = datetime.today() - timedelta(days=1)
    one_day = today + timedelta(days=1)
    #thirty_days = today + timedelta(days=30)
    #a = User.objects.get(id=id)
    b = Doctors.objects.get(email=data.email)
    context = {'doctorscount': appointment.objects.filter(p_doctor=b.id).count(),
               'patientscount': appointment.objects.filter(p_doctor=b.id).filter(p_appointmentDate__range= [today, one_day]).count(),
               'pt': appointment.objects.filter(p_doctor=b.id).filter(p_appointmentDate__range=[today, one_day]),
               'patients': PatientInfo.objects.all().order_by('-id')[:5]}

    return render(request,'Doctor/dash.html',context)
def profile(request):
    data = request.user
    context= {'doctor': Doctors.objects.get(email=data.email)}
    return render(request, 'Doctor/profile.html',context)
def editprofile(request, email):
    context = {}
    obj = get_object_or_404(Doctors, email=email)
    form = DoctorprofileForm(request.POST or None, request.FILES or None, instance=obj)
    if form.is_valid():
        form.save()
        return redirect("/dr_profile/")
    context["form"] = form

    return render(request, "Doctor/createprofile.html", context)

def patients(request):
    formdata = PatientInfo()
    data = request.user
    context = {'form':formdata}
    pt = PatientInfo.objects.get()  # Collect all records from table
    return render(request, 'Doctor/patients.html', {'pt': pt})
def addpatient(request):
    return render(request,'Doctor/addpatient.html')
def editpatient(request, id):
    patient = PatientInfo.objects.get(id=id)
    return render(request, 'Doctor/editpatient.html', {'patient': patient})
def delete_patient(request, id):
    PatientInfo.objects.get(id=id).delete()
    return redirect('/dr_patients/')


def appointments(request):
    #formdata = appointment()
    data = request.user
    #a = User.objects.get(id=id)
    b= Doctors.objects.get(email=data.email)
    context = {'pt':appointment.objects.filter(p_doctor=b.id)}  # Collect records from table
    return render(request,'Doctor/appointments.html',context)
def dr_delete_appointment(request, id):
    appointment.objects.get(id=id).delete()
    return redirect('/dr_appointments/')

def edit_appointments(request, id):
    context = {}
    obj = get_object_or_404(appointment, id=id)
    form = AppointForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
        return redirect('/dr_appointments/')
    context["form"] = form

    return render(request, 'Doctor/edit_appointments.html', context)
def today_appointments(request,id):
    today = datetime.today()-timedelta(days=1)
    thirty_days = today + timedelta(days=30)
    a = User.objects.get(id=id)
    b = Doctors.objects.get(email=a.email)
    context = {'pt': appointment.objects.filter(p_doctor=b.id).filter(p_appointmentDate__range= [today, thirty_days])
               }

    #context = {'appointment':appointment.objects.filter(p_appointmentDate = today)}
    return render(request, 'Doctor/today_appointments.html', context)

def add_appointments(request, email):
    form = AddAppointmentForm()
    #a = User.objects.get(id=id)
    #b = Doctors.objects.get(email=a.email)
    #c = Doctors.objects.filter(id=b.id)
    c = Doctors.objects.get(email=email)
    if request.method == "POST":
        form = AddAppointmentForm(request.POST)
        if form.is_valid():
            p_name = form.cleaned_data['p_name']
            p_email = form.cleaned_data['p_email']
            p_phone = form.cleaned_data['p_phone']
            p_appointmentDate = form.cleaned_data['p_appointmentDate']
            #p_specialization = form.cleaned_data['p_specialization']
            #p_doctor = form.cleaned_data['p_doctor']
            p_timeslot = form.cleaned_data['p_timeslot']
            p_message = form.cleaned_data['p_message']
            appointment(p_name=p_name, p_email=p_email, p_phone=p_phone, p_appointmentDate=p_appointmentDate, p_specialization_id=c.specialties_id, p_doctor_id=c.id , p_timeslot=p_timeslot, p_message=p_message).save()
        return redirect('/dr_appointments/')
    return render(request,'Doctor/add_appointments.html',{'form': form})
def schedule(request):
    return render(request,'Doctor/schedule.html')
def edit_schedule(request):
    return render(request,'Doctor/edit_schedule.html')
def add_schedule(request):
    return render(request,'Doctor/add_schedule.html')
def reports(request):
    return render(request,'Doctor/reports.html')
def prescription(request):
    context = {'pt': Prescription.objects.all()}
    return render(request,'Doctor/prescription.html', context)
def add_medicine(request):
    if request.method=='POST':
        medicine_name = request.POST['medicinename']
        medicine_timing = request.POST['medicinetiming']
        medicine_quantity = request.POST['medicinequantity']
        Prescription(medicine_name=medicine_name,medicine_timing=medicine_timing,medicine_quantity=medicine_quantity).save()
        return redirect('/dr_prescription/')
    return render(request, 'Doctor/add_medicine.html')


def dr_logout(request):
    auth.logout(request)
    return redirect('/login/')
def dr_bank_details(request):
    return render(request,'Doctor/dr_bank_details.html')
