from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import RegexValidator
from django.contrib.auth.models import AbstractUser
# Create your models here.
class User(AbstractUser):
    middle_name=models.CharField(max_length=64)
    is_doctor=models.BooleanField(default=False)
    is_pharmacist=models.BooleanField(default=False)
    is_labassistance=models.BooleanField(default=False)



class Department(models.Model):
    department_name = models.CharField(max_length=100)
    Head_dept=models.ForeignKey('self',blank=True,null=True,on_delete=models.CASCADE)
    description = models.CharField(max_length=700)


    def __str__(self):
        return self.department_name
class specializations(models.Model):
    specializations_name=models.CharField(max_length=64)
    specializations_desc = models.CharField(max_length=256)
    def __str__(self):
        return self.specializations_name

class degree(models.Model):
    degree_name=models.CharField(max_length=64)
    degree_desc=models.CharField(max_length=256)
    def __str__(self):
        return self.degree_name
class Doctors(models.Model):
    GENDER_CHOICES = (
        ('Female', 'Female',),
        ('Male', 'Male',),
        ('Unsure', 'Unsure',),
    )
    first_name = models.CharField(max_length=64)
    middle_name=models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    email = models.EmailField(max_length=254,unique=True)
    age = models.IntegerField()
    gender = models.CharField(max_length=30,choices=GENDER_CHOICES,)
    address = models.CharField(max_length=1024)
    address2=models.CharField(max_length=256)
    city = models.CharField(max_length=40)
    state = models.CharField(max_length=40)
    postal_code= models.IntegerField()
    phone=models.CharField(max_length=50,unique=True)
    phone2=models.CharField(max_length=50,unique=True,blank=True)
    home_no=models.CharField(max_length=50,blank=True)
    photo = models.ImageField(upload_to='doctorimg')
    Certificate = models.FileField(upload_to='doctor_certificate')
    bio = models.CharField(max_length=1024)
    qulifications = models.ForeignKey(degree,on_delete=models.CASCADE)
    specialties = models.ForeignKey(specializations,on_delete=models.CASCADE,verbose_name='select specializations')
    year_of_experience=models.FloatField(blank=True)
    hospital_name=models.CharField(max_length=64,blank=True)
    job_position=models.CharField(max_length=64,blank=True)
    bank_Account_no=models.CharField(max_length=50,unique=True)
    ifsc_code=models.CharField(max_length=64)
    bank_name=models.CharField(max_length=64)
    documents=models.FileField(upload_to='dr_documents')
    consulting_fee = models.IntegerField()
    department=models.ForeignKey(Department,on_delete=models.CASCADE)


class labtests(models.Model):
    test_name=models.CharField(max_length=40)
    test_code= models.CharField(max_length=16,unique=True)
    price = models.FloatField(default=0)
    lab_no = models.IntegerField(null=True)
    description = models.CharField(max_length=700)

    def __str__(self):
        return self.test_name

class staff(models.Model):
    ROLE_CHOICE=(
        ('Laboratorist','Laboratorist'),('Phamacist','Phamacist'),

    )
    GENDER_CHOICES = (
        ('Female', 'Female',),
        ('Male', 'Male',),
        ('Unsure', 'Unsure',),
    )
    first_name = models.CharField(max_length=64)
    middle_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    email=models.EmailField(max_length=128)
    age = models.IntegerField()
    gender = models.CharField(max_length=30,choices=GENDER_CHOICES,)
    joining_date= models.DateField(verbose_name='Date of join', auto_now_add=False, auto_now=False)
    phone=models.CharField(validators=[phone_regex], max_length=17,unique=True)
    phone2=models.CharField(validators=[phone_regex], max_length=17,blank=True)
    role=models.CharField(max_length=20,choices=ROLE_CHOICE)
    emp_img= models.ImageField(upload_to='emp_img')
    address = models.CharField(max_length=1024)
    address2 = models.CharField(max_length=256)
    city = models.CharField(max_length=40)
    state = models.CharField(max_length=40)
    postal_code = models.IntegerField()
    qulifications = models.ForeignKey(degree, on_delete=models.CASCADE)
    year_of_experience = models.FloatField(blank=True)
    hospital_name = models.CharField(max_length=64, blank=True)
    job_position = models.CharField(max_length=64, blank=True)
    bank_Account_no = models.CharField(max_length=50, unique=True)
    ifsc_code = models.CharField(max_length=64)
    bank_name = models.CharField(max_length=64)
    documents = models.FileField(upload_to='staff_documents')
    Certificate = models.FileField(upload_to='staff_certificate')
    def __str__(self):  # __unicode__ for Python 2
        return self.staff_user_id.username

class staffExperience(models.Model):
    staff_id=models.IntegerField(null=True)
    staff_mail=models.EmailField(max_length=256,blank=True)
    year_of_experience = models.FloatField(blank=True)
    hospital_name = models.CharField(max_length=64, blank=True)
    job_position = models.CharField(max_length=64, blank=True)





    




