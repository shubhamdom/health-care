
from django.urls import path,include
from django.contrib.auth import views as auth_views
from . import views


app_name = 'Admin'
urlpatterns = [
    path('adminlogin/',views.adminLogin,name='adminlogin'),
    path('admin_register/',views.adminRegister,name='adminregister'),
    path('changepassword2/',views.changePassword,name='changepassword2'),
    path('adminlogout/',views.adminLogout,name='adminlogout'),
    path('resetpassword/',views.resetPassword,name='resetpassword'),
    path('verifyotp/',views.verifyOtp,name='verifyotp'),
    path('changepassword/',views.changePass,name='changepassword'),
    path('profile/',views.profile,name='profile'),
    path('edit_profile/',views.editProfile,name='edit_profile'),
    path('admindashboard/',views.admindashboard,name='admindashboard'),
    path('doctors/',views.doctors,name='doctors'),
    path('add_doctor/',views.addDoctor,name='add_doctor'),
    path('editDoctor/<int:drid>/',views.updateDoctor,name='editDoctor'),
    path('deletedoctor/<int:phone>/',views.deleteDoctor,name='deletedoctor'),
    path('searchdoctor/',views.searchDctor,name="searchdoctor"),
    path('singledoctorview/<int:drid>',views.singleDoctorView,name='singledoctorview'),
    path('patients/',views.patients,name='patients'),
    path('addpatient/',views.addPatients,name='addpatient'),
    path('deletepatient/<int:pid>/',views.deletePatients,name='deletepatient'),
    path('updatepatients/<int:pid>',views.updatePatients,name='updatepatients'),
    path('appointments/',views.appointments,name='appointments'),
    path('addappointment/',views.addappointment,name="addappointment"),
    path('departmentsview/',views.departmentsview,name='departmentsview'),
    path('add_departments/',views.addDepartments,name='add_departments'),
    path('editdepartment/<int:id>/',views.editDepartment,name='editdepartment'),
    path('deletdept/<int:deptid>/', views.deletedept, name='deletdept'),
    path('addsubdepartment/',views.addSubDepartmet,name='adsubdepartment'),
    path('lab_tests/',views.labTests,name='lab_tests'),
    path('addtest/',views.addTests,name='addtest'),
    path('editlabtests/<int:id>/',views.editLabTests,name='editlabtests'),
    path('deletelabtest/<int:labid>/',views.deletelab,name='deletelabtest'),
    path('employee/',views.employee,name='employee'),
    path('add_employee/',views.addemployee,name='add_employee'),
    path('delete_employee/<int:id>',views.deleteEmployee,name='delete_employee'),
    path('edit_employee/<int:id>',views.editEmployee,name='edit_employee'),
    path('viewspecializationsdegree/',views.viewSpecializationAndDegree,name='viewspecializationsdegree'),
    path('add_specializations/',views.addspecializations,name='add_specializations'),
    path('add_degree/',views.addDegree,name='add_degree'),
    path('deletespecialization/<int:id>/',views.deletespecilizations,name='deletespecializarion'),
    path('deletedegree/<int:id>/',views.deleteDegree,name='deletedegree'),
    path('searchappointment/',views.searchAppointment,name='searchappointment'),
    path('searchpatients/',views.searchPatients,name='searchpatients'),
    path('password_reset/',auth_views.PasswordResetView.as_view(),name='password_reset'),
    path('password_reset/done/',auth_views.PasswordResetDoneView.as_view(),name='password_reset_done'),
    path('reset/<uidb64>/<token>/',auth_views.PasswordResetConfirmView.as_view(),name='password_reset_confirm'),
    path('reset/done/',auth_views.PasswordResetCompleteView.as_view(),name='password_reset_complete'),



]