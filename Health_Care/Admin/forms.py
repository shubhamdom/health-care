from django import forms
from django.forms import ModelForm, PasswordInput,Textarea
from .models import Doctors,Department,labtests,staff,specializations,degree,User
from django.forms import ClearableFileInput
import re
from Patient.models import PatientInfo,appointment

class DoctorForm(forms.ModelForm):
    class Meta:
        model = Doctors
        fields = {'first_name','middle_name','last_name','email','age','gender','address','address2','city','state','postal_code',
                  'qulifications','specialties','phone','phone2','home_no','photo','Certificate','bio','year_of_experience','hospital_name',
                  'job_position','bank_Account_no','ifsc_code','bank_name','documents','consulting_fee','department'}
        widgets = {
            'bio':Textarea(),
            'Certificate':ClearableFileInput(attrs={'multiple': True}),
            'documents':ClearableFileInput(attrs={'multiple': True}),
            #'email': errors_message={'required':'Enter email '},
        }

    def clean_email(self):
        email = self.cleaned_data['email']
        regex="^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$"
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("Email already exists")
        elif re.search(regex, email)==None:
            raise forms.ValidationError("Enter Correct Email")
        return email
    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')
        if ' ' in first_name:
            raise  forms.ValidationError("Space is not allowed")
        elif first_name.isalpha()==False:
            raise forms.ValidationError("Enter only Charecter")
        return first_name
    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name')
        if ' ' in last_name:
            raise  forms.ValidationError("Space is not allowed")
        elif last_name.isalpha()==False:
            raise forms.ValidationError("Enter only Charecter")
        return last_name
    def clean_age(self):
        age = self.cleaned_data.get('age')
        if age<=22 or age>=65:
            raise forms.ValidationError("Enter Correct age")
        return age
    def clean_city(self):
        city = self.cleaned_data.get('city')
        try:
            city.encode('ascii')
        except UnicodeDecodeError:
            raise forms.ValidationError("Enter valid city name or only Charecter")
        if not city.replace(' ', '').isalpha():
            raise forms.ValidationError("Enter valid city name or only Charecter")
        return city
    def clean_state(self):
        state = self.cleaned_data.get('state')
        if state.isalpha() == False:
            raise forms.ValidationError("Enter valid city name or only Charecter")
        return state
    def clean_postal_code(self):
        postal_code=self.cleaned_data.get('postal_code')
        if len(str(postal_code))!=6:
            raise forms.ValidationError("Enter 6 digit valid postal Code ")
        return postal_code
    def clean_phone(self):
        phone=self.cleaned_data.get('phone')
        regex=("^[0-9]{10}$")
        if re.search(regex, phone) == None:
            raise forms.ValidationError("Enter Correct 10 digit Phone no")
        elif Doctors.objects.filter(phone=phone).exists():
            raise forms.ValidationError("Phone no is already registered")
        return phone
    def clean_bio(self):
        bio=self.cleaned_data.get('bio')
        if bio.count(' ') < 24:
            raise forms.ValidationError("Minimum 25 word are required ")
        return bio

    field_order = ['first_name','last_name','email','age','gender','address','city','state','postal_code','qulifications','specialties','phone','photo','Certificat','bio']
class DepartmentForm(forms.ModelForm):
    class Meta:
        model = Department
        fields = {'department_name','description'}
        widgets ={
            'description':Textarea()
        }

    field_order = ['department_name','description']

class LabTestsForm(forms.ModelForm):
    class Meta:
        model = labtests
        fields={'test_name','test_code','price','description'}
        widgets = {
            'description': Textarea()
        }
    def clean_test_name(self):
        name = self.cleaned_data.get('test_name')
        if name.isalpha() == False:
            raise forms.ValidationError("Enter valid test name or only Charecter")
        return name
    def clean_test_code(self):
        test_code = self.cleaned_data.get('test_code')
        if labtests.objects.filter(test_code=test_code).exists():
            raise forms.ValidationError("test is already Exits")
        return test_code


    field_order = ['test_name','test_code','price','description']

class signupForm(forms.ModelForm):
    class Meta:
        model=User
        fields={'password'}
        widgets = {
                        "password": PasswordInput(attrs={'placeholder':'Abcd@143','autocomplete': 'off','data-toggle': 'password'}),
        }

    def clean_password(self):

        password = self.cleaned_data.get('password')
        if len(password) < 8:
            raise forms.ValidationError("Make sure your password is at lest 8 letters")
        elif re.search('[0-9]', password) is None:
            raise forms.ValidationError("Make sure your password has a number in it")
        elif re.search('[A-Z]', password) is None:
            raise forms.ValidationError("Make sure your password has a capital letter in it")
        return password

    field_order = ['first_name','last_name','password']
class staffForm(forms.ModelForm):
    class Meta:
        model=staff
        fields={'first_name','middle_name','last_name','email','phone','age','phone2','gender','role','emp_img','joining_date',
                'address','address2','city','state','postal_code','qulifications','year_of_experience','hospital_name',
                'job_position','bank_Account_no','ifsc_code','bank_name','documents','Certificate'}
        widgets = {
            'phone':forms.TextInput(attrs={'placeholder': 'Enter Mobile number'}),
            'joining_date':forms.TextInput(
                attrs={'type': 'date'}
            )
        }
        labels = {
            'emp_img': 'Profile photo',
            'year_of_experience':'Experience(year)',
            'job_position':'Position',
            'documents':'Documents(Adhar card/Pan Card/Driving License)'
        }

    def clean_email(self):
        email = self.cleaned_data['email']
        regex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$"
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("Email already exists")
        elif staff.objects.filter(email=email).exists():
            raise forms.ValidationError("Email already exists")
        elif re.search(regex, email) == None:
            raise forms.ValidationError("Enter Correct Email")
        return email

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')
        if ' ' in first_name:
            raise forms.ValidationError("Space is not allowed")
        elif first_name.isalpha() == False:
            raise forms.ValidationError("Enter only Charecter")
        return first_name
    def clean_middle_name(self):
        middle_name = self.cleaned_data.get('middle_name')
        if ' ' in middle_name:
            raise forms.ValidationError("Space is not allowed")
        elif middle_name.isalpha() == False:
            raise forms.ValidationError("Enter only Charecter")
        return middle_name

    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name')
        if ' ' in last_name:
            raise forms.ValidationError("Space is not allowed")
        elif last_name.isalpha() == False:
            raise forms.ValidationError("Enter only Charecter")
        return last_name

    def clean_phone(self):
        phone = self.cleaned_data.get('phone')
        regex = ("^[0-9]{10}$")
        if re.search(regex, phone) == None:
            raise forms.ValidationError("Enter Correct 10 digit Phone no")
        elif Doctors.objects.filter(phone=phone).exists():
            raise forms.ValidationError("Phone no is already registered")
        return phone
    def clean_age(self):
        age = self.cleaned_data.get('age')
        if age<=20 or age>=80:
            raise forms.ValidationError("Enter Correct age")
        return age
    def clean_city(self):
        city = self.cleaned_data.get('city')
        try:
            city.encode('ascii')
        except UnicodeDecodeError:
            raise forms.ValidationError("Enter valid city name or only Charecter")
        if not city.replace(' ', '').isalpha():
            raise forms.ValidationError("Enter valid city name or only Charecter")
        return city
    def clean_state(self):
        state = self.cleaned_data.get('state')
        if state.isalpha() == False:
            raise forms.ValidationError("Enter valid state name or only Charecter")
        return state
    def clean_postal_code(self):
        postal_code=self.cleaned_data.get('postal_code')
        if len(str(postal_code))!=6:
            raise forms.ValidationError("Enter 6 digit valid postal Code ")
        return postal_code
    def clean_bank_no(self):
        account_no=self.cleaned_data.get('bank_Account_no')
        regex = ("^[0-9]{9,18}$")
        if re.search(regex,account_no) == None:
            raise forms.ValidationError("Enter Correct Bank Account Number")
        return account_no



class specializationsForm(forms.ModelForm):
    class Meta:
        model=specializations
        fields={'specializations_name','specializations_desc'}
        widgets = {
            'specializations_desc': Textarea()
        }
        labels = {
            'specializations_desc': 'specializations description'
        }

    field_order = ['specializations_name','specializations_desc']


class degreeForm(forms.ModelForm):
    class Meta:
        model=degree
        fields={'degree_name','degree_desc'}
        widgets = {
            'degree_desc': Textarea()
        }
        labels = {
            'degree_desc': 'degree description'
        }

    field_order = ['degree_name','degree_desc']


class patientForm(forms.ModelForm):
    class Meta:
        model=PatientInfo
        fields={'p_firstname','p_middlename','p_lastname','p_email','p_phone','p_age','p_address','patient_pic'}
    field_order = ['p_firstname','p_middlename','p_lastname','p_email','p_phone','p_age','p_address','patient_pic']


class appointmentForm(forms.ModelForm):
    class Meta:
        model = appointment
        fields =  {'p_firstname','p_middlename','p_lastname','p_email','p_phone','p_appointmentDate','p_specialization','p_doctor','p_timeslot','p_message'}

        labels = {
            'p_firstname':'First Name',
            'p_middlename':'Middle Name',
            'p_lastname':'Last Name',
            'p_email':'Email',
            'p_phone':'Phone',
            'p_appointmentDate':'Date',
            'p_specialization' : 'Specialization',
            'p_doctor' :'Doctor',
            'p_timeslot':'Timeslot',
            'p_message' :'Message'

        }

        widgets={
            'p_firstname':forms.TextInput(attrs={'placeholder': 'Your Name'}),
            'p_email':forms.TextInput(attrs={'placeholder':'Email'}),
            'p_phone':forms.TextInput(attrs={'placeholder': 'Phone'}),
            'p_appointmentDate':forms.TextInput(attrs={'type':'date'}),
            'p_message':forms.Textarea(attrs={'placeholder': 'Message'}),
       }

class subdepartmentForm(forms.ModelForm):
    class Meta:
        model=Department
        fields={'department_name','Head_dept','description'}

class changePasswordForm(forms.Form):
    old_password_flag = True  # Used to raise the validation error when it is set to False

    old_password = forms.CharField(label="Old Password", min_length=6, widget=forms.PasswordInput())
    new_password= forms.CharField(label='New Password', min_length=6, widget=forms.PasswordInput())
    re_new_password = forms.CharField(label="Re-type New Password", min_length=6, widget=forms.PasswordInput())

    def set_old_password_flag(self):
        # This method is called if the old password entered by user does not match the password in the database, which sets the flag to False
        self.old_password_flag = False
        return 0
    def clean_old_password(self, *args, **kwargs):
        old_password = self.cleaned_data.get('old_password')
        if not old_password:
            raise forms.ValidationError("You must enter your old password.")
        if self.old_password_flag == False:
            # It raise the validation error that password entered by user does not match the actucal old password.
            raise forms.ValidationError("The old password that you have entered is wrong.")
        return old_password


