from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.contrib.auth.models import User, auth
from .forms import DoctorForm,DepartmentForm,LabTestsForm,signupForm,staffForm,specializationsForm,degreeForm,patientForm,appointmentForm,subdepartmentForm,changePasswordForm
from .models import Department,Doctors,labtests,staff,User,specializations,degree
from Patient.models import PatientInfo,appointment
from Doctor.forms import DoctorprofileForm
from django.contrib.auth.hashers import make_password
from django_otp.oath import hotp
from django.core.cache import cache
from django.utils.timezone import datetime #important if using timezones
from datetime import date, timedelta,datetime
from django.contrib import messages
from django.conf import settings
from django.core.mail import send_mail
from twilio.rest import Client
from django.contrib.postgres.search import SearchVector
import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger



# Create your views here.
secret_key = b'1234567890123467890'
def adminLogin(request):
    if request.method=="POST":
        uname = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(request, username=uname, password=password)
        if user is not None:
            if User.objects.filter(is_superuser=True):
                auth.login(request, user)
                return redirect('/admindashboard/')
    cache.clear()
    return render(request,'admin/login.html')
#admin registeration view
def adminRegister(request):
    if request.method=='POST':
        fname= request.POST['fname']
        lname = request.POST['lname']
        email=request.POST['email']
        password=request.POST['password']

        if User.objects.filter(email=email).exists():
            pass
        else:
            User.objects.create_user(first_name=fname, last_name=lname, username=email, email=email,
                                     password=password,is_superuser = True).save()
            return redirect('/adminlogin/')
    return render(request, 'admin/register.html')
#admin logout view
def adminLogout(request):
    auth.logout(request)
    return redirect('/adminlogin/')

def otpGenration():
    for counter in range(5):
        otp = hotp(key=secret_key, counter=counter, digits=6)
        return(otp)
#forgot password view
def resetPassword(request):
    if request.method=="POST":
        email=request.POST['email']
        user = User.objects.filter(email=email)
        if user is not None:
            otp=otpGenration()
            subject = 'forgot your password'
            message = f'Your one time passssord is {otp}' \
                      f' Thank you'
            email_from = settings.DEFAULT_FROM_EMAIL
            recipient_list = [email]
            send_mail(subject, message, email_from, recipient_list)
            return redirect('/verifyotp/')
        else:
            pass
            
    return render(request,'admin/forgot-password.html')
def verifyOtp(request):
    if request.method=="POST":
        otp=resetPassword.otp
        email=resetPassword.email
        u_otp=request.POST['otp']
        if u_otp==otp:
            return redirect('/changepassword/',email)
    return render(request,'admin/verify_otp.html')
def changePass(request):
    if request.method=="POST":
        pass1= request.POST['password1']
        pass2 = request.POST['password2']
        email=resetPassword.email
        if pass1==pass2:
            user=User.objects.get(username=email)
            user.set_password(pass1)
            user.save()
            return redirect('/adminlogin/')
    return render(request,'admin/changepassword.html')
def sendmail(fname,lname,email,password):
    subject = 'welcome to Health care + world'
    message = f'Hi {fname} {lname}. ' \
              f'Your login details are ' \
              f'username: {email}  and ' \
              f' password: {password} ' \
              f'you can login in our system by above details'
    email_from = settings.DEFAULT_FROM_EMAIL
    recipient_list = [email]
    send_mail(subject, message, email_from, recipient_list)
def sendSms(fname,lname,phone,email,password):
    message_to_broadcast = ("Hii..%s Welcome to health Care +  "%(fname) %(lname),
                                                "your Login details is:"
                            "Username: %s"%email,
                            "Password:" %password,
                            "Go to the hospital site and check it"
                            "Thank you!..")
    client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
    for recipient in phone:
        if recipient:
            client.messages.create(to=recipient,
                                   from_=settings.TWILIO_NUMBER,
                                   body=message_to_broadcast)
#function for admin Dashboard view
def admindashboard(request):
    context={'doctorscount':Doctors.objects.all().count(),
             'patientscount': PatientInfo.objects.all().count(),
             'doctors':Doctors.objects.all().order_by('-id')[:5],
             'patients':PatientInfo.objects.all().order_by('-id')[:5],
             'departments': Department.objects.all().order_by('id')[:10],
             'tests': labtests.objects.all().order_by('id')[:10]}

    return render(request,'admin/dashboard.html',context)
#function for Doctor view
def doctors(request):
    doctors = Doctors.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(doctors, 12)
    try:
        doctors = paginator.page(page)
    except PageNotAnInteger:
        doctors = paginator.page(1)
    except EmptyPage:
        doctors = paginator.page(paginator.num_pages)
    context = {'doctors': doctors}
    return render(request, 'admin/doctors.html', context)
def searchDctor(request):
    search_vector = SearchVector('first_name', 'middle_name', 'last_name','email','phone','phone2')

    if ('doctorName' in request.GET) and request.GET['doctorName'].strip():
        query_string = request.GET['doctorName']
        doctors = Doctors.objects.annotate(search=search_vector).filter(search=query_string)

    else:
        doctors = None
    context = {'doctors': doctors}
    return render(request, 'admin/doctors.html', context)
#adding Doctors
def addDoctor(request):
    if request.method == 'POST':
        form = DoctorForm(request.POST, request.FILES)
        sform = signupForm(request.POST)
        if form.is_valid():
            if sform.is_valid():
                email = form.cleaned_data['email']
                fname=form.cleaned_data['first_name']
                lname=form.cleaned_data['last_name']
                password=sform.cleaned_data['password']
                phone=form.cleaned_data['phone']
                data=form.save(commit=False)
                data.save()
                User.objects.create_user(username=email,email=email,password=password,first_name=fname,last_name=lname,is_staff=True,is_doctor=True)

                sendmail(fname,lname,email,password)
                #sendSms(fname,lname,phone,email,password)
                return redirect('/doctors/')
        else:
            return render(request, 'admin/add_doctor.html', {'form': form, 'sform': sform})


    form = DoctorForm()


    sform = signupForm()

    return render(request, 'admin/add_doctor.html', {
        'form': form,'sform':sform
    })
def updateDoctor(request,drid):
    context = {}
    obj = get_object_or_404(Doctors,id=drid)
    form = DoctorprofileForm(request.POST or None,request.FILES or None, instance=obj)
    if form.is_valid():
        form.save()
        return redirect('/doctors/')
    context["form"] = form


    return render(request,'admin/edit_doctor.html',context)
#delete doctor view
def deleteDoctor(request,phone):
    dr = Doctors.objects.get(phone=phone)
    username=dr.email
    User.objects.filter(username=username).delete()
    Doctors.objects.filter(phone=phone).delete()

    return redirect('/doctors/')
#patients view
def patients(request):
    patient=PatientInfo.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(patient, 10)
    try:
        patients = paginator.page(page)
    except PageNotAnInteger:
        patients = paginator.page(1)
    except EmptyPage:
        patients = paginator.page(paginator.num_pages)
    context={'patients':patients}
    return render(request,'admin/patients.html',context)
# delete patients view
def deletePatients(request,pid):
    data = PatientInfo.objects.get(id=pid)
    pemail= data.p_email
    User.objects.filter(email=pemail).delete()
    PatientInfo.objects.filter(id=pid).delete()
    messages.success(request, 'Successfully Deleted the Patients !')
    return redirect('/patients/')
def updatePatients(request,pid):
    context = {}
    obj = get_object_or_404(PatientInfo, id=pid)
    form = patientForm(request.POST or None, request.FILES or None, instance=obj)
    if form.is_valid():
        email = form.cleaned_data['p_email']
        fname = form.cleaned_data['p_firstname']
        mname = form.cleaned_data['p_middlename']
        lname = form.cleaned_data['p_lastname']
        """user=User.objects.filter(email=email)
        user.first_name=fname
        user.middle_name=mname
        user.last_name=lname
        user.email=email
        user.save()"""
        form.save()
        return redirect('/patients/')
    context["form"] = form

    return render(request, 'admin/edit_patient.html', context)
#appointments view
def appointments(request):
    today = datetime.date.today() - timedelta(days=1)
    thirty_days = today + timedelta(days=30)
    appointments=appointment.objects.select_related('p_doctor').filter(p_appointmentDate__range=[today, thirty_days])
    page = request.GET.get('page', 1)
    paginator = Paginator(appointments, 10)
    try:
        appointments = paginator.page(page)
    except PageNotAnInteger:
        appointments = paginator.page(1)
    except EmptyPage:
        appointments = paginator.page(paginator.num_pages)
    context = {'appointment':appointments}
    return render(request,'admin/appointments.html',context)
def addappointment(request):
    if request.method=="POST":
        form=appointmentForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Successfully Added the appointment!')
            return redirect('/appointments/')
        else:
            return render(request, 'admin/addappointment.html', {'form': form})

    apptform=appointmentForm()
    return render(request,'admin/addappointment.html',{'form':apptform})
#department view
def departmentsview(request):
    context = {'departments': Department.objects.all()}
    return render(request,'admin/departments.html',context)
#add departments view
def addDepartments(request):
    if request.method=='POST':
        Dept_name = request.POST['dname']
        desc = request.POST['desc']
        Department(department_name=Dept_name,description=desc).save()
        messages.success(request, 'Successfully Added the department!')
        return redirect('/departmentsview/')
    return render(request,'admin/add_departments.html')
def deletedept(request,deptid):
    Department.objects.filter(id=deptid).delete()
    messages.success(request, 'Successfully deleted the department!')
    return redirect('/departmentsview/')
def editDepartment(request,id):
    context={}
    obj = get_object_or_404(Department, id=id)
    form = DepartmentForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
        messages.success(request, 'Successfully updated  the department information!')
        return redirect('/departmentsview/')
    context["form"] = form

    return render(request,'admin/update_department.html', context)
def addSubDepartmet(request):
    if request.method=='POST':
        form=subdepartmentForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Successfully updated  the department information!')
            return redirect('/departmentsview/')
    context={'form':subdepartmentForm()}
    return render(request,'admin/add_sub_department.html', context)
def labTests(request):
    context = {'tests': labtests.objects.all()}
    return render(request,'admin/lab_tests.html',context)
#add tests view
def addTests(request):
    if request.method=='POST':
        form = LabTestsForm(request.POST or None)
        if form.is_valid():
            form.save()
            messages.success(request, 'Successfully Added the LabTest!')
            return redirect('/lab_tests/')
        else:
            return render(request, 'admin/add_test.html', {'form':form})

    context={'form':LabTestsForm()}
    return render(request,'admin/add_test.html',context)
def editLabTests(request,id):
    context={}
    obj = get_object_or_404(labtests, id=id)
    form = LabTestsForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
        messages.success(request, 'Successfully updated the LabTest information!')
        return redirect('/lab_tests/')
    context["form"] = form

    return render(request,'admin/update_lab_tests.html', context)
#delete lab view
def deletelab(request,labid):
    labtests.objects.filter(id=labid).delete()
    messages.success(request, 'Successfully deleted the LabTest!')
    return redirect('/lab_tests/')
#profile view
def profile(request):
    return render(request,'admin/profile.html')
#edit profile view
def editProfile(request):
    return render(request,'admin/edit_profile.html')
def employee(request):
    context={'staffs':staff.objects.all()}
    return render(request,'admin/employee.html',context)
def addemployee(request):
    if request.method == 'POST':
        sform = signupForm(request.POST)
        eform = staffForm(request.POST, request.FILES)

        if eform.is_valid():
            if sform.is_valid():
                fname = eform.cleaned_data['first_name']
                mname=eform.cleaned_data['middle_name']
                lname = eform.cleaned_data['last_name']
                email = eform.cleaned_data['email']
                password =sform.cleaned_data['password']
                eform.save()
                User.objects.create_user(username=email, email=email, password=password, first_name=fname, last_name=lname,middle_name=mname,is_staff=True)
                messages.success(request, 'Successfully Added the Employee!')
                return redirect('/employee/')
        else:

            return render(request, 'admin/add_employee.html', {'sform': sform, 'eform': eform})


    
    context={'sform':signupForm(),'eform':staffForm()}
    return render(request,'admin/add_employee.html',context)


def deleteEmployee(request,id):
    data = staff.objects.get(id=id)
    email= data.email
    User.objects.filter(email=email).delete()
    staff.objects.filter(id=id).delete()
    messages.success(request, 'Successfully Deleted the Employee!')
    return redirect('/employee/')

def editEmployee(request,id):
    context = {}
    obj = get_object_or_404(staff, id=id)
    form = staffForm(request.POST or None, instance=obj)
    if form.is_valid():
        """emp = staff.objects.get(id=id)
        email=emp.email
        fn = form.cleaned_data['first_name']
        ln = form.cleaned_data['last_name']
        User.objects.get(username=email).update(first_name=fn,last_name=ln)"""
        form.save()
        return redirect('/employee/')
    context["eform"] = form
    return render(request,'admin/edit_employee.html',context)

def viewSpecializationAndDegree(request):
    context={
        'specializations':specializations.objects.all(),
        'degrees':degree.objects.all()
    }
    return render(request, 'admin/view_specializations_degree.html', context)
def addspecializations(request):
    if request.method=='POST':
        form=specializationsForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Successfully Added the specialite!')
            return redirect('/add_specializations/')
    form=specializationsForm()
    return render(request, 'admin/add_specializations.html',{'form':form})

def addDegree(request):
    if request.method == 'POST':
        form = degreeForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Successfully Added the degree!')
            return redirect('/add_degree/')
    form = degreeForm()
    return render(request, 'admin/add_degree.html', {'form': form})

def deletespecilizations(request,id):
    specializations.objects.filter(id=id).delete()
    messages.success(request, 'Successfully Delete the specializations!')
    return redirect('/viewspecializationsdegree/')
def deleteDegree(request,id):
    degree.objects.filter(id=id).delete()
    messages.success(request, 'Successfully Delete the degree!')
    return redirect('/viewspecializationsdegree/')
def addPatients(request):
    if request.method == 'POST':
        form=patientForm(request.POST,request.FILES)
        sform=signupForm(request.POST)
        if form.is_valid():
            if sform.is_valid():
                email = form.cleaned_data['p_email']
                firstname = form.cleaned_data['p_firstname']
                middlename = form.cleaned_data['p_middlename']
                lastname = form.cleaned_data['p_lastname']
                password = sform.cleaned_data['password']
                form.save()
                User.objects.create_user(username=email,password=password,email=email,first_name=firstname,last_name=lastname,middle_name=middlename)
                return redirect('/patients/')
    eform=patientForm()
    sform=signupForm()
    return render(request, 'admin/add_patient.html',{'eform':eform,'sform':sform})
def singleDoctorView(request,drid):
    context={'doctor':Doctors.objects.get(id=drid)}
    return render(request,'admin/singleDoctorView.html',context)

def changePassword(request):
    form = changePasswordForm(request.POST or None)

    old_password = request.POST.get("old_password")
    new_password = request.POST.get("new_password")
    re_new_password = request.POST.get("re_new__password")

    if request.POST.get("old_password"):

        user = User.objects.get(username=request.user.username)

        # User entered old password is checked against the password in the database below.
        if user.check_password('{}'.format(old_password)) == False:
            form.set_old_password_flag()

    if form.is_valid():

        user.set_password('{}'.format(new_password))
        user.save()
        messages.success(request, 'Successfully Change the Password')
        return redirect('/admindashboard/')

    return render(request, 'admin/password.html', {"form": form})

def searchAppointment(request):
    search_vector = SearchVector('p_firstname', 'p_middlename', 'p_lastname')

    if ('patientName' in request.GET) and request.GET['patientName'].strip():
        query_string = request.GET['patientName']
        appointment1 = appointment.objects.annotate(search=search_vector).filter(search=query_string)

    else:
        appointment1=None
    return render(request, 'admin/appointments.html', {'appointment': appointment1})
def searchPatients(request):
    search_vector = SearchVector('p_firstname', 'p_middlename', 'p_lastname','p_email','p_phone')
    if ('patientName' in request.GET) and request.GET['patientName'].strip():
        query_string = request.GET['patientName']
        if query_string=="":
            patients = PatientInfo.objects.all()
        patients = PatientInfo.objects.annotate(search=search_vector).filter(search=query_string)

    else:
        patients=None
    return render(request, 'admin/patients.html', {'patients': patients})










   



